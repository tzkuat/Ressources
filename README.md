# A list of several resources on different areas such as security, OSINT, DevOps, etc. 

This list is composed of tools that I use or have used and is not intended to become an "Awesome-xxx" type list.

## Disponible resources : 

* [OSINT](https://gitlab.com/tzkuat/Ressources/blob/master/OSINT.md)
* [Security](https://gitlab.com/tzkuat/Ressources/blob/master/Security.md)
* [Privacy](https://gitlab.com/tzkuat/Ressources/blob/master/Privacy.md)
* [Awesome List](https://gitlab.com/tzkuat/Ressources/blob/master/Awesome-list.md)
* [Cryptography](https://gitlab.com/tzkuat/Ressources/blob/master/Crypto.md)

## Cheat Sheet : 

* [GnuPG](https://gitlab.com/tzkuat/Ressources/blob/master/gpg-cheatsheet.md)
* [OpenSSL](https://gitlab.com/tzkuat/Ressources/blob/master/openssl-cheatsheet.md)
